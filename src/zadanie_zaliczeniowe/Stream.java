package zadanie_zaliczeniowe;

import java.util.List;

public class Stream {
	public void displayList(List<Pralka> pralki) {
		System.out.println("Lista pralek przed sortowaniem:");
		pralki.stream().forEach(e -> System.out.println(e.getProducent()));
	}

	public void sortList(List<Pralka> pralki) {
		System.out.println("\n" + "Lista pralek po sortowaniu:");
		pralki.stream().sorted((a, b) -> a.getProducent().compareTo(b.getProducent()))
				.forEach(e -> System.out.println(e.getProducent()));
	}

}
