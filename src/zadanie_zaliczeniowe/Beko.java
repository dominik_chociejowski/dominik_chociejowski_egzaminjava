package zadanie_zaliczeniowe;

public class Beko extends Pralka {

	public Beko() {
		super.producent = Producent.BEKO;
		super.setTempStep(1);
		super.temperatureSetup = 1;
	}

	public Beko(int currentProgram, double currentTemp, int actualWhirlSpeed) throws MyException {
		super(1, currentTemp, actualWhirlSpeed, Producent.BEKO);
		super.temperatureSetup = 1.0;
		super.setTempStep(1);
		setProgram(currentProgram);
	}
}
