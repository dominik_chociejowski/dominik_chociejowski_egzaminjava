package zadanie_zaliczeniowe;

public class Amica extends Pralka {

	public Amica() {
		super.producent = Producent.AMICA;
	}

	public Amica(int currentProgram, double currentTemp, int actualWhirlSpeed) throws MyException {
		super(1, currentTemp, actualWhirlSpeed, Producent.AMICA);
		setProgram(currentProgram);
	}
}
