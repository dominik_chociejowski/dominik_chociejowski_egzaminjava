package zadanie_zaliczeniowe;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws MyException {

		List<Pralka> pralki = new ArrayList<>();

		Whirlpool whirlpool = new Whirlpool(22, 25.6, 1000);
		System.out.println(whirlpool.getProducent());
		whirlpool.showStatus();
		whirlpool.previousProgram();
		whirlpool.showStatus();
		whirlpool.upV();
		whirlpool.showStatus();
		whirlpool.downV();
		whirlpool.showStatus();
		whirlpool.tempUp();
		whirlpool.showStatus();
		whirlpool.tempDown();
		whirlpool.showStatus();

		Beko beko = new Beko(20, 50.5, 0);

		System.out.println(beko.getProducent());
		beko.showStatus();
		beko.previousProgram();
		beko.showStatus();
		beko.upV();
		beko.showStatus();
		beko.downV();
		beko.showStatus();
		beko.tempUp();
		beko.showStatus();
		beko.tempDown();
		beko.showStatus();

		Amica amica = new Amica(5, 80, 1000);
		System.out.println(amica.getProducent());
		amica.showStatus();
		amica.previousProgram();
		amica.showStatus();
		amica.upV();
		amica.showStatus();
		amica.downV();
		amica.showStatus();
		amica.tempUp();
		amica.showStatus();
		amica.tempDown();
		amica.showStatus();

		pralki.add(whirlpool);
		pralki.add(beko);
		pralki.add(amica);

		Stream stream = new Stream();
		stream.displayList(pralki);
		stream.sortList(pralki);

	}
}
