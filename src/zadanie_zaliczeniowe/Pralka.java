package zadanie_zaliczeniowe;

public abstract class Pralka {
	private int currentProgram;
	private int minProgramNo = 1;
	private int maxProgramNo = 20;

	private double currentTemp;
	private double minTemp = 0;
	private double maxTemp = 90;
	private double tempStep = 0.5;
	protected double temperatureSetup;
	
	private int actualWhirlSpeed;
	private int minWhirl = 0;
	private int maxWhirl = 1000;
	private int whirlStep = 100;

	public Producent producent;

	public Pralka() {

	}

	public Pralka(double temperatureSetup , double currentTemp, int actualWhirlSpeed, Producent producent)
			throws MyException {
		this.temperatureSetup = temperatureSetup;
		setTemp(currentTemp);
		setV(actualWhirlSpeed);
		this.producent = producent;
	}

	public void setProgram(int program) throws MyException {
		if (program <= maxProgramNo && program >= minProgramNo) {
			currentProgram = program;
		} else {
			throw new MyException("Program poza zasiegiem");
		}
	}

	public int getProgram() {
		return currentProgram;
	}

	public void nextProgram() {
		if (currentProgram >= minProgramNo && currentProgram < maxProgramNo) {
			currentProgram++;
		} else {
			currentProgram = minProgramNo;
		}
	}

	public void previousProgram() {
		if (currentProgram > minProgramNo && currentProgram <= maxProgramNo) {
			currentProgram--;
		} else {
			currentProgram = maxProgramNo;
		}
	}

	public void setTemp(double temp) {
		if (temp > maxTemp) {
			currentTemp = maxTemp;
			System.out.println("Przekroczono zakres - temperatura ustawiona na MAXIMUM");
		} else if (temp < minTemp) {
			currentTemp = minTemp;
			System.out.println("Przekroczono zakres - temperatura ustawiona na MINIMUM");
		} else {
			currentTemp = Math.round(temp * temperatureSetup) / temperatureSetup;
		}
	}

	public double getTemp() {
		return currentTemp;
	}

	public void tempUp() {
		if (currentTemp >= maxTemp) {
			currentTemp = maxTemp;
			System.out.println("Przekroczono zakres" + "\n" + "Aktualna temperatura " + currentTemp + (char) 176 + "C");
		} else {
			currentTemp += tempStep;
			System.out.println("Aktualna temperatura " + currentTemp + "�C");
		}
	}

	public void tempDown() {
		if (currentTemp <= minTemp) {
			currentTemp = minTemp;
			System.out.println("Przekroczono zakres" + "\n" + "Aktualna temperatura " + currentTemp + (char) 176 + "C");
		} else {
			currentTemp -= tempStep;
			System.out.println("Aktualna temperatura " + currentTemp + "�C");
		}
	}

	public void setV(int speed) throws MyException {
		if (speed % whirlStep != 0) {
			throw new MyException("Liczba nie jest wielokrotnoscia 100");
		} else {
			if (speed > maxWhirl || speed < minWhirl) {
				throw new MyException("Obroty poza zasiegiem");
			} else {
				actualWhirlSpeed = speed;
			}
		}
	}

	public int getV() {
		return actualWhirlSpeed;
	}

	public void upV() {
		actualWhirlSpeed += whirlStep;
		if (actualWhirlSpeed > maxWhirl) {
			actualWhirlSpeed = minWhirl;
		}
	}

	public void downV() {
		actualWhirlSpeed -= whirlStep;
		if (actualWhirlSpeed < minWhirl) {
			actualWhirlSpeed = maxWhirl;
		}
	}

	public void setMaxProgramNo(int maxProgramNo) {
		this.maxProgramNo = maxProgramNo;
	}

	public void setTempStep(double tempStep) {
		this.tempStep = tempStep;
	}

	public void setProducent(Producent producent) {
		this.producent = producent;
	}

	public Producent getProducent() {
		return producent;
	}

	public void showStatus() {
		System.out.println("Program NO: " + currentProgram + ", temperatura: " + currentTemp
				+ "�C, predkosc wirowania: " + actualWhirlSpeed + " RPM.");
	}

}
