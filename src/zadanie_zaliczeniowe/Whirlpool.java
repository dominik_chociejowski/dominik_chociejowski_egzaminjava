package zadanie_zaliczeniowe;

public class Whirlpool extends Pralka {

	public Whirlpool() {
		super.producent = Producent.WHIRLPOOL;
		super.setMaxProgramNo(25);
	}

	public Whirlpool(int currentProgram, double currentTemp, int actualWhirlSpeed) throws MyException {
		super(1, currentTemp, actualWhirlSpeed, Producent.WHIRLPOOL);
		super.setMaxProgramNo(25);
		setProgram(currentProgram);
	}
}